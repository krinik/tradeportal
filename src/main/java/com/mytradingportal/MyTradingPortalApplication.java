package com.mytradingportal;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Stream;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import com.mytradingportal.model.AccountType;
import com.mytradingportal.model.Customer;
import com.mytradingportal.model.CustomerMonthlyDetails;
import com.mytradingportal.model.CustomerProfile;
import com.mytradingportal.repository.CustomerRespository;

@SpringBootApplication
public class MyTradingPortalApplication {

	public static void main(String[] args) {
		SpringApplication.run(MyTradingPortalApplication.class, args);
	}

	@Bean
	CommandLineRunner init(CustomerRespository customerRepository) {
		return args -> {
			Stream.of("John").forEach(customerDetails -> {

				Map<Integer, Double> dayMap = new LinkedHashMap<>();

				for (int i = 1; i <= 31; i++) {
					dayMap.put(i, 100000.0);
				}

//				CustomerMonthlyDetails Jandet = new CustomerMonthlyDetails("JAN", dayMap, 100, 100);
//				CustomerMonthlyDetails Febdet = new CustomerMonthlyDetails("FEB", dayMap, 200, 300);
//				CustomerMonthlyDetails mardet = new CustomerMonthlyDetails("MAR", dayMap, 300, 600);
//				CustomerMonthlyDetails aprdet = new CustomerMonthlyDetails("APR", dayMap, 400, 1000);
//				CustomerMonthlyDetails maydet = new CustomerMonthlyDetails("MAY", dayMap, 500, 1500);
//				CustomerMonthlyDetails jundet = new CustomerMonthlyDetails("JUN", dayMap, 600, 2100);
//				CustomerMonthlyDetails juldet = new CustomerMonthlyDetails("JUL", dayMap, 700, 2800);
//				CustomerMonthlyDetails augdet = new CustomerMonthlyDetails("AUG", dayMap, 800, 3600);
//				CustomerMonthlyDetails sepdet = new CustomerMonthlyDetails("SEP", dayMap, 900, 4500);
//				CustomerMonthlyDetails octdet = new CustomerMonthlyDetails("OCT", dayMap, 1000, 5500);
//				CustomerMonthlyDetails novdet = new CustomerMonthlyDetails("NOV", dayMap, 1100, 6600);
//				CustomerMonthlyDetails decdet = new CustomerMonthlyDetails("DEC", dayMap, 1200, 7800);
//
//				List<CustomerMonthlyDetails> custArray = new ArrayList<>();
//				custArray.add(Jandet);
//				custArray.add(Febdet);
//				custArray.add(mardet);
//				custArray.add(aprdet);
//				custArray.add(maydet);
//				custArray.add(jundet);
//				custArray.add(juldet);
//				custArray.add(augdet);
//				custArray.add(sepdet);
//				custArray.add(octdet);
//				custArray.add(novdet);
//				custArray.add(decdet);
//
//				CustomerProfile profile = new CustomerProfile("Hoskerehalli, Banglore", "9742157307", "tradeLoginId", 
//						"nikhil@gmail.com", "emailPassword", "backOfficePassword", "tradingPassword", 
//						"investorPassword", "01-05-2020", "Cash");
//				
//				Customer customer = new Customer("111111", "Nikhil", AccountType.CENT, 1000, 1000, "Nikhil", "1000", profile, custArray);
//				
////				Customer custome1 = new Customer("222", "Krishna", AccountType.PRIME, 10000, "", 10000,
//						"", "Nothing", custArray);
//				Customer custome2 = new Customer("333", "Lepakshi", AccountType.PRIME, 10000, "", 10000,
//						"", "Nothing", custArray);
				
				//Customer cu = new Customer("3", "Lepakshi", AccountType.CENT, 1000, 1000, "Remarks", "Address", "9742157307", 
					//	"tradeLoginId", "emailId", "password", "backOfficePassword", "tradingPassword", "investorPassword", "joiningDate", "payouts", "paymentMode", custArray);

				//customerRepository.save(customer);
//				customerRepository.save(custome1);
//				customerRepository.save(custome2);
			});

		};
	}

}
