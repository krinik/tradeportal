package com.mytradingportal.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

import com.mytradingportal.model.Customer;


public interface CustomerRespository extends MongoRepository<Customer, Integer>{

	//Supports native JSON query string
    @Query("{clientId:'?0'}")
    Customer findCustomerDetailsByClientId(String clientId);
    

}
