package com.mytradingportal.service;

import java.util.List;

import com.mytradingportal.model.Customer;

public interface CustomerService {
	
	void save(Customer customer);
	
	List<Customer> update(Customer customer);
}
