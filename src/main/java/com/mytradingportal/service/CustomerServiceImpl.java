package com.mytradingportal.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mytradingportal.model.AccountType;
import com.mytradingportal.model.Customer;
import com.mytradingportal.model.CustomerMonthlyDetails;
import com.mytradingportal.model.CustomerProfile;
import com.mytradingportal.model.MonthDetails;
import com.mytradingportal.repository.CustomerRespository;

@Service
public class CustomerServiceImpl implements CustomerService {

	@Autowired
	private CustomerRespository customerRepository;

	@Override
	public void save(Customer customer) {
		if (customer != null) {

			customer = setupCustomer(customer);
			customer = calculateProfit(customer);

			if (customerRepository.findCustomerDetailsByClientId(customer.getClientId()) != null) {
				if (customer.getAccountType().toString().equalsIgnoreCase("CENT"))
					customer.setAccountType(AccountType.CENT);
				else
					customer.setAccountType(AccountType.PRIME);
			}
		}
		customerRepository.save(customer);
	}

	@Override
	public List<Customer> update(Customer customer) {

		// calculate total customers profit
		double totalCustomersProfit = 0;
		List<Customer> custList = (List<Customer>) customerRepository.findAll();
		for (Customer cust : custList) {
			totalCustomersProfit += cust.getAnnualReturns();
			cust.setTotalCustomersProfit(totalCustomersProfit);
		}
		//

		Customer existingCust = customerRepository.findCustomerDetailsByClientId(customer.getClientId());
		CustomerProfile profile = customer.getCustomerProfile();
		List<CustomerMonthlyDetails> monthlyDetails = customer.getMonthlyDetails();

		if (existingCust != null) {
			// update monthly details
			existingCust.setMonthlyDetails(monthlyDetails);
			existingCust = calculateProfit(existingCust);
			// update profile
			existingCust.setClientName(customer.getClientName());
			profile.setJoiningDate(profile.getJoiningDate());
			profile.setAddress(profile.getAddress());
			profile.setPhoneNumber(profile.getPhoneNumber());
			profile.setTradeLoginId(profile.getTradeLoginId());
			profile.setEmailId(profile.getEmailId());
			profile.setPassword(profile.getPassword());
			profile.setBackOfficePassword(profile.getBackOfficePassword());
			profile.setTradingPassword(profile.getTradingPassword());
			profile.setInvestorPassword(profile.getInvestorPassword());
			existingCust.setCustomerProfile(profile);
			customerRepository.save(existingCust);
		}
		return customerRepository.findAll();

	}

	private Customer setupCustomer(Customer customer) {

		HashMap<Integer, Double> oddMonth = new HashMap<Integer, Double>();
		for (int i = 1; i <= 31; i++) {
			oddMonth.put(i, 0.0);
		}

		HashMap<Integer, Double> evenMonth = new HashMap<Integer, Double>();
		for (int i = 1; i <= 30; i++) {
			evenMonth.put(i, 0.0);
		}

		HashMap<Integer, Double> febMonth = new HashMap<Integer, Double>();
		for (int i = 1; i <= 29; i++) {
			febMonth.put(i, 0.0);
		}

		MonthDetails jun2020Details = new MonthDetails("June 2020", 0, evenMonth, false);
		MonthDetails jul2020Details = new MonthDetails("July 2020", 1, oddMonth, false);
		MonthDetails aug2020Details = new MonthDetails("August 2020", 1, oddMonth, false);
		MonthDetails sep2020Details = new MonthDetails("September 2020", 0, evenMonth, false);
		MonthDetails oct2020Details = new MonthDetails("October 2020", 1, oddMonth, false);
		MonthDetails nov2020Details = new MonthDetails("November 2020", 0, evenMonth, false);
		MonthDetails dec2020Details = new MonthDetails("December 2020", 1, oddMonth, false);

		MonthDetails jan2021Details = new MonthDetails("January 2021", 1, oddMonth, false);
		MonthDetails feb2021Details = new MonthDetails("February 2021", -1, febMonth, false);
		MonthDetails mar2021Details = new MonthDetails("March 2021", 1, oddMonth, false);
		MonthDetails apr2021Details = new MonthDetails("April 2021", 0, evenMonth, false);
		MonthDetails may2021Details = new MonthDetails("May 2021", 1, oddMonth, false);
		MonthDetails june2021Details = new MonthDetails("June 2021", 0, oddMonth, false);
		MonthDetails july2021Details = new MonthDetails("July 2021", 1, oddMonth, false);
		MonthDetails august2021Details = new MonthDetails("August 2021", 1, oddMonth, false);
		MonthDetails september2021Details = new MonthDetails("September 2021", 0, oddMonth, false);
		MonthDetails october2021Details = new MonthDetails("October 2021", 1, oddMonth, false);
		MonthDetails november2021Details = new MonthDetails("November 2021", 0, oddMonth, false);
		MonthDetails december2021Details = new MonthDetails("December 2021", 1, oddMonth, false);

		MonthDetails jan2022Details = new MonthDetails("January 2022", 1, oddMonth, false);

		CustomerMonthlyDetails monthlyDetails_jun2020 = new CustomerMonthlyDetails(jun2020Details, 0, 0);
		CustomerMonthlyDetails monthlyDetails_jul2020 = new CustomerMonthlyDetails(jul2020Details, 0, 0);
		CustomerMonthlyDetails monthlyDetails_aug2020 = new CustomerMonthlyDetails(aug2020Details, 0, 0);
		CustomerMonthlyDetails monthlyDetails_sep2020 = new CustomerMonthlyDetails(sep2020Details, 0, 0);
		CustomerMonthlyDetails monthlyDetails_oct2020 = new CustomerMonthlyDetails(oct2020Details, 0, 0);
		CustomerMonthlyDetails monthlyDetails_nov2020 = new CustomerMonthlyDetails(nov2020Details, 0, 0);
		CustomerMonthlyDetails monthlyDetails_dec2020 = new CustomerMonthlyDetails(dec2020Details, 0, 0);

		CustomerMonthlyDetails monthlyDetails_jan2021 = new CustomerMonthlyDetails(jan2021Details, 0, 0);
		CustomerMonthlyDetails monthlyDetails_feb2021 = new CustomerMonthlyDetails(feb2021Details, 0, 0);
		CustomerMonthlyDetails monthlyDetails_mar2021 = new CustomerMonthlyDetails(mar2021Details, 0, 0);
		CustomerMonthlyDetails monthlyDetails_apr2021 = new CustomerMonthlyDetails(apr2021Details, 0, 0);
		CustomerMonthlyDetails monthlyDetails_may2021 = new CustomerMonthlyDetails(may2021Details, 0, 0);
		CustomerMonthlyDetails monthlyDetails_jun2021 = new CustomerMonthlyDetails(june2021Details, 0, 0);
		CustomerMonthlyDetails monthlyDetails_jul2021 = new CustomerMonthlyDetails(july2021Details, 0, 0);
		CustomerMonthlyDetails monthlyDetails_aug2021 = new CustomerMonthlyDetails(august2021Details, 0, 0);
		CustomerMonthlyDetails monthlyDetails_sep2021 = new CustomerMonthlyDetails(september2021Details, 0, 0);
		CustomerMonthlyDetails monthlyDetails_oct2021 = new CustomerMonthlyDetails(october2021Details, 0, 0);
		CustomerMonthlyDetails monthlyDetails_nov2021 = new CustomerMonthlyDetails(november2021Details, 0, 0);
		CustomerMonthlyDetails monthlyDetails_dec2021 = new CustomerMonthlyDetails(december2021Details, 0, 0);

		CustomerMonthlyDetails monthlyDetails_jan2022 = new CustomerMonthlyDetails(jan2022Details, 0, 0);

		List<CustomerMonthlyDetails> monthlyDetailsList = new ArrayList<>();
		monthlyDetailsList.add(monthlyDetails_jun2020);
		monthlyDetailsList.add(monthlyDetails_jul2020);
		monthlyDetailsList.add(monthlyDetails_aug2020);
		monthlyDetailsList.add(monthlyDetails_sep2020);
		monthlyDetailsList.add(monthlyDetails_oct2020);
		monthlyDetailsList.add(monthlyDetails_nov2020);
		monthlyDetailsList.add(monthlyDetails_dec2020);

		monthlyDetailsList.add(monthlyDetails_jan2021);
		monthlyDetailsList.add(monthlyDetails_feb2021);
		monthlyDetailsList.add(monthlyDetails_mar2021);
		monthlyDetailsList.add(monthlyDetails_apr2021);
		monthlyDetailsList.add(monthlyDetails_may2021);
		monthlyDetailsList.add(monthlyDetails_jun2021);
		monthlyDetailsList.add(monthlyDetails_jul2021);
		monthlyDetailsList.add(monthlyDetails_aug2021);
		monthlyDetailsList.add(monthlyDetails_sep2021);
		monthlyDetailsList.add(monthlyDetails_oct2021);
		monthlyDetailsList.add(monthlyDetails_nov2021);
		monthlyDetailsList.add(monthlyDetails_dec2021);

		monthlyDetailsList.add(monthlyDetails_jan2022);

		customer.setMonthlyDetails(monthlyDetailsList);

		CustomerProfile profile = new CustomerProfile();
		customer.setCustomerProfile(profile);
		return customer;
	}

	private Customer calculateProfit(Customer customer) {

		List<CustomerMonthlyDetails> monthlyDetailsList = customer.getMonthlyDetails();
		for (CustomerMonthlyDetails customerDetails : monthlyDetailsList) {
			customerDetails = setMonthlyProfitByAllDays(customerDetails);
		}

		customer = setAnnualProfitOrReturns(customer);
		customer = setPayouts(customer);
		return customer;
	}

	private Customer setPayouts(Customer customer) {
		double annualProfit = customer.getAnnualReturns();
		double payouts = 0;
		payouts = annualProfit * 5;
		payouts /= 100;
		customer.setPayouts(payouts);
		customer.setPayouts(0);
		return customer;
	}

	private Customer setAnnualProfitOrReturns(Customer customer) {
		double annualProfit = 0;
		List<CustomerMonthlyDetails> custDetails = customer.getMonthlyDetails();
		for (CustomerMonthlyDetails eachCustomerDetail : custDetails) {
			System.out.println(eachCustomerDetail.getMonthlyProfit());
			annualProfit += eachCustomerDetail.getMonthlyProfit();
			System.out.println("Annual profit calculated are " + annualProfit);
		}
		customer.setAnnualReturns(annualProfit);
		System.out.println("Customer annual returns are " + customer.getAnnualReturns());
		return customer;
	}

	private CustomerMonthlyDetails setMonthlyProfitByAllDays(CustomerMonthlyDetails customerDetails) {
		Map<Integer, Double> daysMap = customerDetails.getMonthDetails().getDays();
		double sum = 0;
		for (Entry<Integer, Double> entry : daysMap.entrySet()) {
			sum += entry.getValue();
		}

		customerDetails.setMonthlyProfit(sum);

		return customerDetails;
	}

}
