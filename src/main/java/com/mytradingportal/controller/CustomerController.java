package com.mytradingportal.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.SimpleMongoClientDbFactory;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.mongodb.client.MongoClients;
import com.mytradingportal.model.Customer;
import com.mytradingportal.repository.CustomerRespository;
import com.mytradingportal.service.CustomerService;

@RestController
@CrossOrigin(origins = "http://localhost:4200")
public class CustomerController {

	@Autowired
	private CustomerRespository customerRepository;

	@Autowired
	private CustomerService customerService;

	MongoOperations mongoOps = new MongoTemplate(new SimpleMongoClientDbFactory(MongoClients.create(), "tradeportal"));

	String getCurrencyRates() {
		String uri = "https://api.exchangeratesapi.io/latest";
		RestTemplate restTemplate = new RestTemplate();
		String result = restTemplate.getForObject(uri, String.class);
		return result;

	}

	@GetMapping("/trade/dollarRate")
	String getCurrency() {
		return getCurrencyRates();
	}

	@GetMapping("/trade/customers")
	public List<Customer> getCustomers() {
		System.out.println("Customers list is " + (List<Customer>) customerRepository.findAll());		
		return (List<Customer>) customerRepository.findAll();
	}

	@GetMapping("/trade/validateId/{clientId}")
	public boolean validateCustomerId(@PathVariable("clientId") String clientId) {
		Customer customer = customerRepository.findCustomerDetailsByClientId(clientId);
		if (customer != null) {
			return false;
		}
		return true;
	}

	@PostMapping("/trade/addCustomer")
	void addCustomer(@RequestBody Customer customer) {
		if (customerRepository.findCustomerDetailsByClientId(customer.getClientId()) == null) {
			customerService.save(customer);
		}
	}

	@DeleteMapping("/trade/deleteCustomer/{clientId}")
	List<Customer> deleteCustomer(@PathVariable("clientId") String clientId) {
		Customer customer = customerRepository.findCustomerDetailsByClientId(clientId);
		if (customer != null)
			customerRepository.delete(customer);
		return (List<Customer>) customerRepository.findAll();
	}

	@PostMapping("/trade/customer/{clientId}")
	Customer getCustomerDetails(@PathVariable("clientId") String clientId) {
		Customer cust = customerRepository.findCustomerDetailsByClientId(clientId);
		System.out.println("Customer details for clientId " + clientId + " is " + cust);
		return cust;

	}

	@PutMapping("/trade/updateCustomer")
	List<Customer> updateCustomer(@RequestBody Customer customer) {
		return customerService.update(customer);
	}

}
