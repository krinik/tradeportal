package com.mytradingportal.model;

import java.util.List;

import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Version;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

import com.fasterxml.jackson.core.JsonProcessingException;

@Document(collection = "customer")
public class Customer {

	@Id
	private ObjectId id;

	@Version
	private long version;

	@Indexed(unique = true)
	private String clientId;

	private String clientName;

	private AccountType accountType;

	private double investedAmount;

	private double annualReturns;

	private String remarks;

	private double payouts;

	private String paymentMode;

	private String currencyType;

	private double totalCustomersProfit;

	private CustomerProfile customerProfile;

	public double getTotalCustomersProfit() {
		return totalCustomersProfit;
	}

	public void setTotalCustomersProfit(double totalCustomersProfit) {
		this.totalCustomersProfit = totalCustomersProfit;
	}

	private List<CustomerMonthlyDetails> monthlyDetails;

	public ObjectId getId() {
		return id;
	}

	public void setId(ObjectId id) {
		this.id = id;
	}

	public long getVersion() {
		return version;
	}

	public void setVersion(long version) {
		this.version = version;
	}

	public String getClientId() {
		return clientId;
	}

	public void setClientId(String clientId) {
		this.clientId = clientId;
	}

	public String getClientName() {
		return clientName;
	}

	public void setClientName(String clientName) {
		this.clientName = clientName;
	}

	public AccountType getAccountType() {
		return accountType;
	}

	public void setAccountType(AccountType accountType) {
		this.accountType = accountType;
	}

	public double getInvestedAmount() {
		return investedAmount;
	}

	public void setInvestedAmount(double investedAmount) {
		this.investedAmount = investedAmount;
	}

	public double getAnnualReturns() {
		return annualReturns;
	}

	public void setAnnualReturns(double annualReturns) {
		this.annualReturns = annualReturns;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public double getPayouts() {
		return payouts;
	}

	public void setPayouts(double payouts) {
		this.payouts = payouts;
	}

	public String getPaymentMode() {
		return paymentMode;
	}

	public void setPaymentMode(String paymentMode) {
		this.paymentMode = paymentMode;
	}

	public String getCurrencyType() {
		return currencyType;
	}

	public void setCurrencyType(String currencyType) {
		this.currencyType = currencyType;
	}

	public CustomerProfile getCustomerProfile() {
		return customerProfile;
	}

	public void setCustomerProfile(CustomerProfile customerProfile) {
		this.customerProfile = customerProfile;
	}

	public List<CustomerMonthlyDetails> getMonthlyDetails() {
		return monthlyDetails;
	}

	public void setMonthlyDetails(List<CustomerMonthlyDetails> monthlyDetails) {
		this.monthlyDetails = monthlyDetails;
	}

	public Customer() {
	}

	public Customer(ObjectId id, long version, String clientId, String clientName, AccountType accountType,
			double investedAmount, double annualReturns, String remarks, double payouts, String paymentMode,
			String currencyType, double totalCustomersProfit, CustomerProfile customerProfile,
			List<CustomerMonthlyDetails> monthlyDetails) {
		super();
		this.id = id;
		this.version = version;
		this.clientId = clientId;
		this.clientName = clientName;
		this.accountType = accountType;
		this.investedAmount = investedAmount;
		this.annualReturns = annualReturns;
		this.remarks = remarks;
		this.payouts = payouts;
		this.paymentMode = paymentMode;
		this.currencyType = currencyType;
		this.totalCustomersProfit = totalCustomersProfit;
		this.customerProfile = customerProfile;
		this.monthlyDetails = monthlyDetails;
	}

	@Override
	public String toString() {
		try {
			return new com.fasterxml.jackson.databind.ObjectMapper().writerWithDefaultPrettyPrinter()
					.writeValueAsString(this);
		} catch (JsonProcessingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
		// return new com.google.gson.Gson().toJson(this);
//		return "Customer [id=" + id + ", version=" + version + ", clientId=" + clientId + ", clientName=" + clientName
//				+ ", accountType=" + accountType + ", investedAmount=" + investedAmount + ", annualReturns="
//				+ annualReturns + ", remarks=" + remarks + ", payouts=" + payouts + ", paymentMode=" + paymentMode
//				+ ", currencyType=" + currencyType + ", customerProfile=" + customerProfile + ", monthlyDetails="
//				+ monthlyDetails + "]";
	}

}