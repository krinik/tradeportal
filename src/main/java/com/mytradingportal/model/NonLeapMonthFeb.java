package com.mytradingportal.model;

public enum NonLeapMonthFeb {

	One, Two, Three, Four, Five, Six, Seven, Eight, Nine, Ten, Eleven, Twleve, Thirteen, Fourteen, Fifteen, Sixteen,
	Seventeen, Ninteen, Twenty, TwentyOne, TwentyTwo, TwentyThree, TwentyFour, TwentyFive, TwentySix, TwentySeven,
	TwentyEight, TwentyNine;
}