package com.mytradingportal.model;

public class CustomerProfile {

	private String address;
	
	private String phoneNumber;
	
	private String tradeLoginId;
	
	private String emailId;
	
	private String password;
	
	private String backOfficePassword;
	
	private String tradingPassword;
	
	private String investorPassword;
	
	private String joiningDate;

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public String getTradeLoginId() {
		return tradeLoginId;
	}

	public void setTradeLoginId(String tradeLoginId) {
		this.tradeLoginId = tradeLoginId;
	}

	public String getEmailId() {
		return emailId;
	}

	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getBackOfficePassword() {
		return backOfficePassword;
	}

	public void setBackOfficePassword(String backOfficePassword) {
		this.backOfficePassword = backOfficePassword;
	}

	public String getTradingPassword() {
		return tradingPassword;
	}

	public void setTradingPassword(String tradingPassword) {
		this.tradingPassword = tradingPassword;
	}

	public String getInvestorPassword() {
		return investorPassword;
	}

	public void setInvestorPassword(String investorPassword) {
		this.investorPassword = investorPassword;
	}

	public String getJoiningDate() {
		return joiningDate;
	}

	public void setJoiningDate(String joiningDate) {
		this.joiningDate = joiningDate;
	}

	public CustomerProfile() {}
	public CustomerProfile(String address, String phoneNumber, String tradeLoginId, String emailId, String password,
			String backOfficePassword, String tradingPassword, String investorPassword, String joiningDate) {
		super();
		this.address = address;
		this.phoneNumber = phoneNumber;
		this.tradeLoginId = tradeLoginId;
		this.emailId = emailId;
		this.password = password;
		this.backOfficePassword = backOfficePassword;
		this.tradingPassword = tradingPassword;
		this.investorPassword = investorPassword;
		this.joiningDate = joiningDate;
	}

	@Override
	public String toString() {
		return "CustomerProfile [address=" + address + ", phoneNumber=" + phoneNumber + ", tradeLoginId=" + tradeLoginId
				+ ", emailId=" + emailId + ", password=" + password + ", backOfficePassword=" + backOfficePassword
				+ ", tradingPassword=" + tradingPassword + ", investorPassword=" + investorPassword + ", joiningDate="
				+ joiningDate + "]";
	}	

	
}