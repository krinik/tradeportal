package com.mytradingportal.model;

import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.type.TypeFactory;
import com.fasterxml.jackson.databind.util.Converter;

public class MonthEnumMongoConvertor implements Converter<String, Month> {

	@Override
	public Month convert(String value) {

		for (Month month : Month.values()) {
			if (month.name().equals(value))
				return month;
		}

		return null;
	}

	@Override
	public JavaType getInputType(TypeFactory typeFactory) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public JavaType getOutputType(TypeFactory typeFactory) {
		// TODO Auto-generated method stub
		return null;
	}

}
