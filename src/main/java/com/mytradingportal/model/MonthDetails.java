package com.mytradingportal.model;

import java.util.Map;

public class MonthDetails {

	private String monthName;
	
	private int monthType;

	private Map<Integer,Double> days;
	
	private boolean leapYear = false;

	public MonthDetails(String monthName, int monthType, Map<Integer, Double> days, boolean leapYear) {
		super();
		this.monthName = monthName;
		this.monthType = monthType;
		this.days = days;
		this.leapYear = leapYear;
	}

	public Map<Integer, Double> getDays() {
		return days;
	}

	public String getMonthName() {
		return monthName;
	}

	public int getMonthType() {
		return monthType;
	}

	public boolean isLeapYear() {
		return leapYear;
	}

	public void setDays(Map<Integer, Double> days) {
		this.days = days;
	}

	public void setLeapYear(boolean leapYear) {
		this.leapYear = leapYear;
	}

	public void setMonthName(String monthName) {
		this.monthName = monthName;
	}

	public void setMonthType(int monthType) {
		this.monthType = monthType;
	}

	@Override
	public String toString() {
		return "MonthDetails [monthName=" + monthName + ", monthType=" + monthType + ", days=" + days + ", leapYear="
				+ leapYear + "]";
	}
	
	
}
