package com.mytradingportal.model;

public class CustomerMonthlyDetails {

	MonthDetails monthDetails;

	private double monthlyProfit;

	private double annualProfit;

	public CustomerMonthlyDetails(MonthDetails monthDetails, double monthlyProfit, double annualProfit) {
		super();
		this.monthDetails = monthDetails;
		this.monthlyProfit = monthlyProfit;
		this.annualProfit = annualProfit;
	}

	public double getAnnualProfit() {
		return annualProfit;
	}

	public MonthDetails getMonthDetails() {
		return monthDetails;
	}

	public double getMonthlyProfit() {
		return monthlyProfit;
	}

	public void setAnnualProfit(double annualProfit) {
		this.annualProfit = annualProfit;
	}

	public void setMonthDetails(MonthDetails monthDetails) {
		this.monthDetails = monthDetails;
	}

	public void setMonthlyProfit(double monthlyProfit) {
		this.monthlyProfit = monthlyProfit;
	}

	@Override
	public String toString() {
		return "CustomerMonthlyDetails [monthDetails=" + monthDetails + ", monthlyProfit=" + monthlyProfit
				+ ", annualProfit=" + annualProfit + "]";
	}

}
