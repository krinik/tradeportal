package com.mytradingportal.model;

public enum MonthType {

	EVEN, ODD, LEAP;
}
